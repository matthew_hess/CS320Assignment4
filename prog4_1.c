#include <stdio.h>
#include <string.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
    
    int main (int argc, char *argv[]) 
	{
	printf("Assignment #4-1, Matthew Hess, matthew9510@gmail.com");
	char buff[256];
      int error;
      lua_State *L = luaL_newstate(); 
	luaL_openlibs(L);    
	luaL_dofile(L, argv[1]);
      lua_close(L);
      return 0;
	}
