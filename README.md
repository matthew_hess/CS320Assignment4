# CSAssignment4
## Created by: Matthew Hess


### Project Description:

This forth project is a set 3 programs. Overall the program weeds out the applicants with incorrect outputs on their submitted programs. This project tested my ability to learn how to use the lua language as an interpreter.

---
### prog4_1.c
    This program is written in c, to use the lua library to act as an interpreter.
---
### prog4_2.lua
    This program is written in lua. It runs the FizzBuzz test. 
---
### prog4_3.sh
   This program is written in bash shell scripting.
   
   First the program compiles the lua interpreter properly.
   
   Then It will compare the applicants output to the correct output and displays either "Passed Test" or "Failed Test" as a result. 