#1/bin/bash
echo "Assignment #4-3, Matthew Hess, Matthew9510@gmail.com")

gcc prog4_1.c -llua -ldl -lm -L lua-5.3.3/src -I lua-5.3.3/src

output=`./a.out $1`
correctOutput =`cat $2`
if [ "$output" = "$correctOutput"];
then
    echo "Passed Test"
else
    echo "Failed Test"
fi
